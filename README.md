# Web-based visualization for pysquared pipelines state

## Run natively


Prepare the environment:
```
mamba create -n dj_pipe python=3.12 django -y
mamba activate dj_pipe
```

Set the path to JSON file of pipeline state as env variable ``STATE_JSON_PATH`` and then run the server
```
export STATE_JSON_PATH=/a/b/c/state.json
python manage.py runserver
```

## Build and run with Apptainer

Build the container and make it convenient to call from anywhere:
```
apptainer build pipevis.sif pipevis.def

# Add this into ~/.bashrc
alias pipevis='/path/to/pipevis.sif
```

Run the server from any directory:
```
cd /a/b/c
pipevis state.json
```