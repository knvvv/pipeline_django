from django.shortcuts import render
from django.http import HttpResponse

import os
import json

JSON_PATH = os.getenv('STATE_JSON_PATH')
assert JSON_PATH is not None


def get_data():
    with open(JSON_PATH, 'r') as f:
        return json.load(f)


def homepage(request):
    return render(request, 'home.html', {'data': get_data()})
