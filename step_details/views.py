from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder

import os
import json

JSON_PATH = os.getenv('STATE_JSON_PATH')
assert JSON_PATH is not None

def get_data():
    with open(JSON_PATH, 'r') as f:
        return json.load(f)


def show_details(request, call_id: int):
    full_data = get_data()
    data = full_data[call_id]
    instruction_id, instruction_name, graph_data, scope_state = data

    scope_tree_string = '<div class="keepws">' + '<br/>'.join(
        scope_state.split('\n')) + '</div>'

    nodes_data = json.dumps([{
        'id': node_id,
        'name':
        f"#{node_id} {bytes(cur_instr_name, 'utf-8').decode('unicode_escape')}",
        'color': '#e7298a' if highlighted else '#1b9e77',
        'optimalY': order_value * 50,
        'x': 500,
        'y': 500,
    } for (node_id, cur_instr_name, highlighted,
           order_value) in graph_data['nodes']],
                            cls=DjangoJSONEncoder)
    edges_data = json.dumps([{
        'source': source,
        'target': target
    } for source, target in graph_data['edges']],
                            cls=DjangoJSONEncoder)
    instruction_title = f"Step {call_id}, Instruction #<i>{instruction_id}: {bytes(instruction_name, 'utf-8').decode('unicode_escape')}</i>"
    return render(
        request, 'step_details/details.html', {
            'data': data,
            'call_id': call_id,
            'instruction_title': instruction_title,
            'scope_tree': scope_tree_string,
            'next_call_id':
            call_id + 1 if call_id + 1 < len(full_data) else None,
            'prev_call_id': call_id - 1 if call_id - 1 >= 0 else None,
            'nodes': nodes_data,
            'edges': edges_data,
        })
