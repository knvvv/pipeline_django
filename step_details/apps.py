from django.apps import AppConfig


class StepDetailsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "step_details"
