from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path("<int:call_id>", views.show_details),
]
