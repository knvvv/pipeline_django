import os
import sys


if __name__ == "__main__":
    assert len(sys.argv) == 2, f"Expected precisely one cli argument. Got: {sys.argv}"
    
    input_json_path = sys.argv[1]
    assert os.path.isfile(input_json_path), f"Input JSON file '{input_json_path}' does not exist"
    print(f"Got this JSON path: '{input_json_path}'")

    processed_json_path = os.path.abspath(input_json_path)
    print(f"Full path is '{processed_json_path}'")

    os.environ["STATE_JSON_PATH"] = processed_json_path
    os.chdir('/usr/local/pipevis')
    os.system('python manage.py runserver')
